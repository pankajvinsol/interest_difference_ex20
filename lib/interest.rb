class Interest
  RATE = 0.10

  def initialize
    raise ArgumentError unless block_given?
    data = yield
    @principal, @time = data[:principal], data[:time]
  end

  def simple_interest_amount
    @principal * RATE * @time + @principal
  end

  def compound_interest_amount
    @principal * (1 + RATE) ** @time
  end

  def simple_compound_amount_difference
    (compound_interest_amount - simple_interest_amount).round(2)
  end

end

