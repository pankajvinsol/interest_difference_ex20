require_relative "../lib/interest.rb"

print "Enter principal amount : "
principal = gets.to_f
print "Enter time in year : "
time = gets.to_f
interest = Interest.new { { :principal => principal, :time => time} }
puts "Amount Difference : #{ interest.simple_compound_amount_difference }"

